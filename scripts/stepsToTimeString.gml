///stepsToTimeString(steps, milliseconds)

var output = "";

if(is_undefined(argument0)) {
    return "";
}

output = stringPad(floor(argument0 / (60*60)), 2) + ":" + stringPad(floor(argument0 / 60), 2);

if(argument1) {
    output += "." + stringPad(floor(argument0 * (100/60)), 2);
}

return output;
