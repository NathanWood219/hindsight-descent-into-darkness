///Put this script in the step event of any solid block that the player can collide with

if!(global.draw_menu) and (instance_number(obj_player)>0)
    {if!(place_meeting(x,y-1,obj_spikes)) and (place_meeting(x,y-5,obj_player))
        {mask_index=spr_floor_mask_large}
    else if!(place_meeting(x,y+1,obj_spikes)) and (place_meeting(x,y+5,obj_player))
        {mask_index=spr_floor_mask_large}
    else
        {mask_index=spr_floor_mask}
    }
