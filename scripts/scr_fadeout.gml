//fading out
if!(global.player_dead) {
    inst=instance_position(x,y,obj_collision_box)
    dist=inst.image_speed;
    alpha=(dist/60);
    
    if(global.round_two) {
        if(dist<=60 and alpha<image_alpha) {
            image_alpha=alpha;
        }
    } else if(dist<=60) {
        image_alpha=alpha;
    } else if!(global.wrapping)
        {image_alpha=1;
    }
} else if(!global.round_two and image_alpha<=1) {
    image_alpha+=0.02;
}
