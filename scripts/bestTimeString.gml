///bestTimeString(level)

var time;

time = ds_grid_get(global.hardcore_levels, argument0, 0);

if(time == 0) {
    return "N/A";
} else {
    return stepsToTimeString(time, true);
}
