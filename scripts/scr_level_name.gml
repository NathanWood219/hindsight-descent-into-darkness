///Level number

//if(room=rm_menu)        {global.level=-1}
//if(room=rm_intro)       {global.level=-1}
if(room=rm_level01)     {global.level=0}
if(room=rm_level0)      {global.level=1}
if(room=rm_level1)      {global.level=2}
if(room=rm_level2)      {global.level=3}
if(room=rm_level3)      {global.level=4}
if(room=rm_level4)      {global.level=5}
if(room=rm_level5)      {global.level=6}
if(room=rm_level6)      {global.level=7}
if(room=rm_level7)      {global.level=8}
if(room=rm_level8)      {global.level=9}
if(room=rm_level9)      {global.level=10}
if(room=rm_level10)     {global.level=11}
if(room=rm_level11)     {global.level=12}
if(room=rm_level12)     {global.level=13}
if(room=rm_level13)     {global.level=14}
if(room=rm_level14)     {global.level=15}
if(room=rm_level15)     {global.level=16}
if(room=rm_level16)     {global.level=17}
if(room=rm_level17)     {global.level=18}
if(room=rm_level18)     {global.level=19}
if(room=rm_level19)     {global.level=20}
if(room=rm_level20)     {global.level=21}
if(room=rm_level21)     {global.level=22}
if(room=rm_level22)     {global.level=23}
if(room=rm_level23)     {global.level=24}
if(room=rm_transition0)     {global.level=25}
if(room=rm_transition)      {global.level=26}

///Room name

if!(global.round_two)
    {switch(global.level)
        {
        case 0: global.level_name="Introduction 1"; break;
        case 1: global.level_name="Introduction 2"; break;
        case 2: global.level_name="Level 1"; break;
        case 3: global.level_name="Level 2"; break;
        case 4: global.level_name="Level 3"; break;
        case 5: global.level_name="Level 4"; break;
        case 6: global.level_name="Level 5"; break;
        case 7: global.level_name="Level 6"; break;
        case 8: global.level_name="Level 7"; break;
        case 9: global.level_name="Level 8"; break;
        case 10: global.level_name="Level 9"; break;
        case 11: global.level_name="Level 10"; break;
        case 12: global.level_name="Level 11"; break;
        case 13: global.level_name="Level 12"; break;
        case 14: global.level_name="Level 13"; break;
        case 15: global.level_name="Level 14"; break;
        case 16: global.level_name="Level 15"; break;
        case 17: global.level_name="Level 16"; break;
        case 18: global.level_name="Level 17"; break;
        case 19: global.level_name="Level 18"; break;
        case 20: global.level_name="Level 19"; break;
        case 21: global.level_name="Level 20"; break;
        case 22: global.level_name="Level 21"; break;
        case 23: global.level_name="Level 22"; break;
        case 24: global.level_name="Level 23"; break;
        case 25: global.level_name="The Descent"; break; //change these to the proper level numbers
        case 26: global.level_name="The Hole"; break;
        
        }
    }
else
    {switch(global.level)
        {
        case 0: global.level_name="Introduction I"; break;
        case 1: global.level_name="Introduction II"; break;
        case 2: global.level_name="Level I"; break;
        case 3: global.level_name="Level II"; break;
        case 4: global.level_name="Level III"; break;
        case 5: global.level_name="Level IV"; break;
        case 6: global.level_name="Level V"; break;
        case 7: global.level_name="Level VI"; break;
        case 8: global.level_name="Level VII"; break;
        case 9: global.level_name="Level VIII"; break;
        case 10: global.level_name="Level IX"; break;
        case 11: global.level_name="Level X"; break;
        case 12: global.level_name="Level XI"; break;
        case 13: global.level_name="Level XII"; break;
        case 14: global.level_name="Level XIII"; break;
        case 15: global.level_name="Level XIV"; break;
        case 16: global.level_name="Level XV"; break;
        case 17: global.level_name="Level XVI"; break;
        case 18: global.level_name="Level XVII"; break;
        case 19: global.level_name="Level XVIII"; break;
        case 20: global.level_name="Level XIX"; break;
        case 21: global.level_name="Level XX"; break;
        case 22: global.level_name="Level XXI"; break;
        case 23: global.level_name="Level XXII"; break;
        case 24: global.level_name="Level XXIII"; break;
        case 25: global.level_name="The Ascent"; break; //change these to the proper level numbers
        case 26: global.level_name="The Hole"; break;
        
        }
    }
