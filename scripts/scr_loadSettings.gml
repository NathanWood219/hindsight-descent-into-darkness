if(!file_exists("settings.ini")) {
    exit;
}

ini_open("settings.ini");

//changing option variables
global.fireflies=ini_read_real("options","fireflies",true);
global.particles=ini_read_real("options","particles",true);
global.body_parts=ini_read_real("options","body_parts",true);
global.fast_spawn=ini_read_real("options","fast_spawn",false);
global.fullscreen=ini_read_real("options","fullscreen",false);
global.extra_bool1=ini_read_real("options","vibration",true);
global.snd_volume=ini_read_real("options","snd_volume",1);
global.msc_volume=ini_read_real("options","msc_volume",1);

ini_close();
