global.spike_challenge=false; global.amnesia=true; //removing spike challenge stuff

if(global.level_select=0)
    {if(global.select1=0)
        {global.round_two=false; global.amnesia=true}
    else
        {global.round_two=false; global.amnesia=false}
    }
else
    {
    if(global.select1=0) //if round one is selected, play that
        {global.round_two=false; global.amnesia=true}
    else if(global.select1=1) //if round_two is selected, play that version instead
        {global.round_two=true; global.amnesia=true}
    else if(global.select1=2) //disabled amnesia
        {global.round_two=false; global.amnesia=false}
    else if(global.select1=3) //round two and disabled amnesia
        {global.round_two=true; global.amnesia=false}
    }

global.prev_x=0;

global.draw_menu=false; global.menu=0; global.select=0; global.start_playing=true;
global.intro_scene=false;

//going through all the levels
if(global.level_select=0)
    {global.start_playing=true; global.intro_scene=true; room_goto(rm_menu)}
else if(global.level_select=1)
    {room_goto(rm_level01)}
else if(global.level_select=2)
    {room_goto(rm_level0)}
else if(global.level_select=3)
    {room_goto(rm_level1)}
else if(global.level_select=4)
    {room_goto(rm_level2)}
else if(global.level_select=5)
    {room_goto(rm_level3)}
else if(global.level_select=6)
    {room_goto(rm_level4)}
else if(global.level_select=7)
    {room_goto(rm_level5)}
else if(global.level_select=8)
    {room_goto(rm_level6)}
else if(global.level_select=9)
    {room_goto(rm_level7)}
else if(global.level_select=10)
    {room_goto(rm_level8)}
else if(global.level_select=11)
    {room_goto(rm_level9)}
else if(global.level_select=12)
    {room_goto(rm_level10)}
else if(global.level_select=13)
    {room_goto(rm_level11)}
else if(global.level_select=14)
    {room_goto(rm_level12)}
else if(global.level_select=15)
    {room_goto(rm_level13)}
else if(global.level_select=16)
    {room_goto(rm_level14)}
else if(global.level_select=17)
    {room_goto(rm_level15)}
else if(global.level_select=18)
    {room_goto(rm_level16)}
else if(global.level_select=19)
    {room_goto(rm_level17)}
else if(global.level_select=20)
    {room_goto(rm_level18)}
else if(global.level_select=21)
    {room_goto(rm_level19)}
else if(global.level_select=22)
    {room_goto(rm_level20)}
else if(global.level_select=23)
    {room_goto(rm_level21)}
else if(global.level_select=24)
    {room_goto(rm_level22)}
else if(global.level_select=25)
    {room_goto(rm_level23)}
