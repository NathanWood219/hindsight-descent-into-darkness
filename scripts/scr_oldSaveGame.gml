///This is obsolete. Use scr_newSaveGame()

ini_open("settings.ini");
    
if(global.fireflies)
    {fireflies=1}
else
    {fireflies=0}

if(global.particles)
    {particles=1}
else
    {particles=0}
    
if(global.body_parts)
    {body_parts=1}
else
    {body_parts=0}
    
if(global.fast_spawn)
    {fast_spawn=1}
else
    {fast_spawn=0}
    
if(global.hardcore_timer)
    {hardcore_timer=1}
else
    {hardcore_timer=0}
    
if(global.fullscreen)
    {fullscreen=1}
else
    {fullscreen=0}

ini_write_real("options","fireflies",fireflies);
ini_write_real("options","particles",particles);
ini_write_real("options","body_parts",body_parts);
ini_write_real("options","fast_spawn",fast_spawn);
ini_write_real("options","hardcore_timer",hardcore_timer);
ini_write_real("options","fullscreen",fullscreen);
ini_write_real("options","snd_volume",global.snd_volume);
ini_write_real("options","msc_volume",global.msc_volume);

ini_close();

// BONUS DATA SECTION //

ini_open("data.ini");

// ----------------  Statistics ---------------- 

if(global.beta_hours<=9)
    {beta_hour_string="0"}
else
    {beta_hour_string=""}

if(global.beta_minutes<=9)
    {beta_min_string="0"}
else
    {beta_min_string=""}
        
if(global.beta_seconds<=9)
    {beta_sec_string="0"}
else
    {beta_sec_string=""}

ini_write_string("statistics","time",beta_hour_string + string(global.beta_hours) + ":" + beta_min_string + string(global.beta_minutes) + ":" + beta_sec_string + string(global.beta_seconds));
ini_write_string("statistics","jumps",string(global.beta_jumps));
ini_write_string("statistics","deaths",string(global.deaths));
ini_write_string("statistics","level_reached",string(global.level_select_max));
ini_write_string("statistics","orbs_collected",string(global.orbs_collected) + " / " + string(global.total_orbs));
ini_write_string("statistics","distance_traveled",string(global.distance_traveled));

// ----------------  Level Statistics ---------------- 

ini_write_string("hardest_levels","first",first_str);
ini_write_string("hardest_levels","second",second_str);
ini_write_string("hardest_levels","third",third_str);
ini_write_string("hardest_levels","fourth",fourth_str);
ini_write_string("hardest_levels","fifth",fifth_str);

// ---------------- Hardcore ----------------

// Current run stats
if!(global.spike_level=-1)
    {
    ini_write_string("hardcore_current","time",hour_string + string(global.hours) + ":" + min_string + string(global.minutes) + ":" + sec_string + string(global.seconds));
    if(global.spike_level=0) {str_level="Introduction 1"}
    else if(global.spike_level=1)  {str_level="Introduction 2"}
    else {str_level="Level " + string(global.spike_level-1)}
    ini_write_string("hardcore_current","level",str_level);
    if(global.spike_round_two) {str="Reverse"}
    else {str="Normal"}
    ini_write_string("hardcore_current","mode",str);
    ini_write_string("hardcore_current","jumps",string(global.spike_jumps));
    ini_write_string("hardcore_current","deaths",string(global.spike_deaths));
    ini_write_string("hardcore_current","orbs",string(global.spike_orbs) + " / " + string(global.total_orbs));
    }
else
    {
    // Clearing the current run if there is none
    ini_write_string("hardcore_current","time","");
    ini_write_string("hardcore_current","level","");
    ini_write_string("hardcore_current","mode","");
    ini_write_string("hardcore_current","jumps","");
    ini_write_string("hardcore_current","deaths","");
    ini_write_string("hardcore_current","orbs","");
    }

// Record run stats (if it exists)
if!(global.spike_level_record=-1)
    {
    ini_write_string("hardcore_record","time",hour_string + string(global.hours_record) + ":" + min_string + string(global.minutes_record) + ":" + sec_string + string(global.seconds_record));
    if(global.spike_level_record=0) {str_level="Introduction 1"}
    else if(global.spike_level_record=1)  {str_level="Introduction 2"}
    else {str_level="Level " + string(global.spike_level_record-1)}
    ini_write_string("hardcore_record","level",str_level);
    if(global.spike_round_two_record) {str="Reverse"}
    else {str="Normal"}
    ini_write_string("hardcore_record","mode",str);
    ini_write_string("hardcore_record","jumps",string(global.spike_jumps_record));
    ini_write_string("hardcore_record","deaths",string(global.spike_deaths_record));
    ini_write_string("hardcore_record","orbs",string(global.spike_orbs_record) + " / " + string(global.total_orbs));
    }
else
    {
    // Clearing the record run if there is none
    ini_write_string("hardcore_record","time","");
    ini_write_string("hardcore_record","level","");
    ini_write_string("hardcore_record","mode","");
    ini_write_string("hardcore_record","jumps","");
    ini_write_string("hardcore_record","deaths","");
    ini_write_string("hardcore_record","orbs","");
    }

// ---------------- Other ---------------- 
if(global.controller=0) {str="Keyboard"}
else                    {str="Xbox Controller"}

ini_write_string("other","control",str);

if(global.bonus_area=0) {str="Not found"}
else                    {str="Found"}

ini_write_string("other","bonus_area",str);
ini_write_string("other","skin",string(global.player_skin));

ini_close();

// END BONUS DATA SECTION //

game_save("hindsight_save");

instance_create(0,0,obj_short_flash);
    
// PATCHING
if(file_exists("patched.txt")) {
    file_delete("patched.txt");
    room_restart();
}
