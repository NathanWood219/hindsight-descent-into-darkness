// 1ST HARDEST LEVEL
deaths0=0; jumps=0; level=0; orb0=0; orb1=0;
for(i=0; i<(global.total_orbs/2)-1; i++)
    {if(global.level_array[i,5]>deaths0)
        {deaths0=global.level_array[i,5]; jumps=global.level_array[i,4]; orb0=global.level_array[i,1]; orb1=global.level_array[i,2]; level=i}
    }
if!(deaths0=0)
    {
    if(level=-1) {str_level="Introduction 1"}
    else if(level=0)  {str_level="Introduction 2"}
    else {str_level="Level " + string(level-1)}
    first_str=str_level + ": " + string(deaths0) + " deaths, " + string(jumps) + " jumps, 1st orb: " + string(orb0) + ", 2nd orb: " + string(orb1);
    }
else
    {first_str=""}

// 2ND HARDEST LEVEL
deaths1=0; jumps=0; level=0; orb0=0; orb1=0;
for(i=0; i<(global.total_orbs/2)-1; i++)
    {if(global.level_array[i,5]>deaths1) and (global.level_array[i,5]<deaths0)
        {deaths1=global.level_array[i,5]; jumps=global.level_array[i,4]; orb0=global.level_array[i,1]; orb1=global.level_array[i,2]; level=i}
    }
if!(deaths1=0)
    {
    if(level=-1) {str_level="Introduction 1"}
    else if(level=0)  {str_level="Introduction 2"}
    else {str_level="Level " + string(level-1)}
    second_str=str_level + ": " + string(deaths1) + " deaths, " + string(jumps) + " jumps, 1st orb: " + string(orb0) + ", 2nd orb: " + string(orb1);
    }
else
    {second_str=""}

// 3RD HARDEST LEVEL
deaths2=0; jumps=0; level=0; orb0=0; orb1=0;
for(i=0; i<(global.total_orbs/2)-1; i++)
    {if(global.level_array[i,5]>deaths2) and (global.level_array[i,5]<deaths0)  and (global.level_array[i,5]<deaths1)
        {deaths2=global.level_array[i,5]; jumps=global.level_array[i,4]; orb0=global.level_array[i,1]; orb1=global.level_array[i,2]; level=i}
    }
if!(deaths2=0)
    {
    if(level=-1) {str_level="Introduction 1"}
    else if(level=0)  {str_level="Introduction 2"}
    else {str_level="Level " + string(level-1)}
    third_str=str_level + ": " + string(deaths2) + " deaths, " + string(jumps) + " jumps, 1st orb: " + string(orb0) + ", 2nd orb: " + string(orb1);
    }
else
    {third_str=""}

// 4TH HARDEST LEVEL
deaths3=0; jumps=0; level=0; orb0=0; orb1=0;
for(i=0; i<(global.total_orbs/2)-1; i++)
    {if(global.level_array[i,5]>deaths3) and (global.level_array[i,5]<deaths0)  and (global.level_array[i,5]<deaths1)  and (global.level_array[i,5]<deaths2)
        {deaths3=global.level_array[i,5]; jumps=global.level_array[i,4]; orb0=global.level_array[i,1]; orb1=global.level_array[i,2]; level=i}
    }
if!(deaths3=0)
    {
    if(level=-1) {str_level="Introduction 1"}
    else if(level=0)  {str_level="Introduction 2"}
    else {str_level="Level " + string(level-1)}
    fourth_str=str_level + ": " + string(deaths3) + " deaths, " + string(jumps) + " jumps, 1st orb: " + string(orb0) + ", 2nd orb: " + string(orb1);
    }
else
    {fourth_str=""}

// 5TH HARDEST LEVEL
deaths4=0; jumps=0; level=0; orb0=0; orb1=0;
for(i=0; i<(global.total_orbs/2)-1; i++)
    {if(global.level_array[i,5]>deaths4) and (global.level_array[i,5]<deaths0)  and (global.level_array[i,5]<deaths1)  and (global.level_array[i,5]<deaths2)  and (global.level_array[i,5]<deaths3)
        {deaths4=global.level_array[i,5]; jumps=global.level_array[i,4]; orb0=global.level_array[i,1]; orb1=global.level_array[i,2]; level=i}
    }
if!(deaths4=0)
    {
    if(level=-1) {str_level="Introduction 1"}
    else if(level=0)  {str_level="Introduction 2"}
    else {str_level="Level " + string(level-1)}
    fifth_str=str_level + ": " + string(deaths4) + " deaths, " + string(jumps) + " jumps, 1st orb: " + string(orb0) + ", 2nd orb: " + string(orb1);
    }
else
    {fifth_str=""}
