//Initial math

var accel, fric;
if (on_ground) {
    accel = S_RUN_ACCEL;
    fric = S_RUN_FRIC;
} else {
    accel = S_AIR_ACCEL;
    fric = S_AIR_FRIC;
}

//Running left initially, and stopping once falling in the hole
if(x>160)
    {if(hspd>0)
        {hspd = approach( hspd, 0, fric )}
        
    hspd = approach( hspd, -S_MAX_H, accel ); 
    image_xscale=-1; standing=false;
    }
else
    {hspd = approach( hspd, 0, fric )}

//Jumping and gravity
if(on_ground)
    {if(place_meeting(x-60,y,obj_solid_parent))
        {vspd = S_JUMP_SPEED; standing=false; crouching=false}
    }
else
    {vspd = approach( vspd, S_MAX_V, S_GRAVITY )}
    
//Rotating as you are falling down the hole
if(y>400)
    {draw_angle-=2}
else
    {draw_angle=0}
