///addHardcoreStat(stat)

var stat, hardcoreLevel;

hardcoreLevel = getHardcoreLevel();

if(!global.hardcore_run && hardcoreLevel = -1) {
    exit;
}

if(global.hardcore_run) {
    if(argument0 == "jumps") {
        global.spike_jumps++;
    } else if(argument0 == "deaths") {
        global.spike_deaths++;
    } else if(argument0 == "orb") {
        global.spike_orbs++;
    }
} else {
    if(argument0 == "bestTime") {
        // Check if best time
        if(ds_grid_get(global.hardcore_levels, hardcoreLevel, 0) == 0 || global.hardcore_steps < ds_grid_get(global.hardcore_levels, hardcoreLevel, 0)) {
            ds_grid_set(global.hardcore_levels, hardcoreLevel, 0, global.hardcore_steps);
        }
        exit;
    } else if(argument0 == "totalTime") {
        // Total time
        ds_grid_add(global.hardcore_levels, hardcoreLevel, 1, global.hardcore_steps);
        exit;
        
    } else if(argument0 == "orb") {
        ds_grid_set(global.hardcore_levels, hardcoreLevel, 2, 1);
        exit;
        
    } else if(argument0 == "jumps") {
        stat = 3;
    } else if(argument0 == "doubleJumps") {
        stat = 4;
    } else if(argument0 == "deaths") {
        stat = 5;
    } else {
        // I like to crash the game personally
        stat = 0;
    }

    ds_grid_add(global.hardcore_levels, hardcoreLevel, stat, 1);
}
