ini_open("savedata.ini");

// Statistics
ini_write_real("Statistics", "Hours", global.beta_hours);
ini_write_real("Statistics", "Minutes", global.beta_minutes);
ini_write_real("Statistics", "Seconds", global.beta_seconds);
ini_write_real("Statistics", "Steps", global.beta_steps);
ini_write_real("Statistics", "Jumps", global.beta_jumps);
ini_write_real("Statistics", "Deaths", global.deaths);
ini_write_real("Statistics", "Orbs", global.orbs_collected);
ini_write_real("Statistics", "Distance", global.distance_traveled);

// Key Bindings
ini_write_real("Bindings", "Left", key_left);
ini_write_real("Bindings", "Right", key_right);
ini_write_real("Bindings", "Up", key_up);
ini_write_real("Bindings", "Down", key_down);
ini_write_real("Bindings", "Jump", key_jump);
ini_write_real("Bindings", "Select", key_select);
ini_write_real("Bindings", "Return", key_return);
ini_write_real("Bindings", "Start", key_start);

// Settings
ini_write_real("Options","Fireflies",global.fireflies);
ini_write_real("Options","Particles",global.particles);
ini_write_real("Options","BodyParts",global.body_parts);
ini_write_real("Options","FastSpawn",global.fast_spawn);
ini_write_real("Options","HardcoreTimer",global.hardcore_timer);
ini_write_real("Options","Fullscreen",global.fullscreen);
ini_write_real("Options","Vibration",global.gamepad_vibration);
ini_write_real("Options","SoundVolume",global.snd_volume);
ini_write_real("Options","MusicVolume",global.msc_volume);

// Achievements
ini_write_real("Achievements", "One", global.achieve_partone);
ini_write_real("Achievements", "Two", global.achieve_parttwo);
ini_write_real("Achievements", "Three", global.achieve_orbs1);
ini_write_real("Achievements", "Four", global.achieve_orbs2);
ini_write_real("Achievements", "Five", global.achieve_hardcore);
ini_write_real("Achievements", "Six", global.achieve_hardcoreorbs1);
ini_write_real("Achievements", "Seven", global.achieve_hardcoreorbs2);
ini_write_real("Achievements", "Eight", global.achieve_hardcoretimed);

// Levels Data
for(i=0; i<26; i++) {
    for(k=0; k<6; k++) {
        // skipping an unused stat
        if(k == 3) {
            k++;
        }
        
        ini_write_real("LevelData", "Level" + string(i+1) + "_" + string(k), global.level_array[i, k]);
    }
}

// Hardcore
ini_write_real("Hardcore", "Level", global.spike_level);
ini_write_real("Hardcore", "Jumps", global.spike_jumps);
ini_write_real("Hardcore", "Deaths", global.spike_deaths);
ini_write_real("Hardcore", "Orbs", global.spike_orbs);
ini_write_real("Hardcore", "OrbTot", global.spike_orb_total);
ini_write_real("Hardcore", "PartTwo", global.spike_round_two);
ini_write_real("Hardcore", "Hours", global.hours);
ini_write_real("Hardcore", "Minutes", global.minutes);
ini_write_real("Hardcore", "Seconds", global.seconds);
ini_write_real("Hardcore", "Steps", global.steps);
ini_write_real("Hardcore", "LevelRec", global.spike_level_record);
ini_write_real("Hardcore", "JumpsRec", global.spike_jumps_record);
ini_write_real("Hardcore", "DeathsRec", global.spike_deaths_record);
ini_write_real("Hardcore", "OrbsRec", global.spike_orbs_record);
ini_write_real("Hardcore", "OrbsTotRec", global.spike_orb_total_record);
ini_write_real("Hardcore", "PartTwoRec", global.spike_round_two_record);
ini_write_real("Hardcore", "HoursRec", global.hours_record);
ini_write_real("Hardcore", "MinutesRec", global.minutes_record);
ini_write_real("Hardcore", "SecondsRec", global.seconds_record);
ini_write_real("Hardcore", "StepsRec", global.steps_record);

// Hardcore levels data
ini_write_string("Hardcore", "LevelsData", ds_grid_write(global.hardcore_levels));
ini_write_real("Hardcore", "HardcoreSelect", obj_menu_control.hardcore_level_select);

// Misc
ini_write_real("Misc", "LastLevel", global.level_select);
ini_write_real("Misc", "LastMode", global.select1);
ini_write_real("Misc", "PlayerSkin", global.player_skin);
ini_write_string("Misc", "GJUser", gj_user);
ini_write_string("Misc", "GJToken", gj_token);
ini_write_real("Misc", "Adventure", global.unlocked_no_amnesia);
ini_write_real("Misc", "Hardcore", global.unlocked_spike_challenge);

ini_close();
