//Slowing down the fall until it comes to a stop

if(y<540)
    {    
    if(vspd>0)
        {vspd-=0.04}
    else
        {vspd=0}
    }

if(y<180 && global.loading) {
    global.loading = false;
}
    
if(vspd<=0) and !(global.cutscene)
    {global.cutscene=true;
    
    gamepad_set_vibration(global.device, 0, 0);
    
    if!(global.spike_challenge) and (global.amnesia)
        {instance_create(0,0,obj_black_flash)}
    }
    
if(global.cutscene) and (instance_number(obj_black_flash)=0)
    {
    scr_vibrateController(vspd/4, vspd/4, 60);
    
    vspd+=0.08;
    
    }
    
if(global.cutscene) and (y<-240)
    {global.unloading = true; global.cutscene=false; global.start_playing=false;
    if(global.spike_challenge)
        {
        // Calculating total steps taken to complete
        time = (global.hours*60*60*60) + (global.minutes*60*60) + (global.seconds*60) + (global.steps);
        record = (global.hours_record*60*60*60) + (global.minutes_record*60*60) + (global.seconds_record*60) + (global.steps_record);
        
        //checking for a new record
        if(time<record) or (global.spike_level>global.spike_level_record)
            {
            //setting new records
            global.spike_level_record=global.spike_level;
            global.spike_jumps_record=global.spike_jumps;
            global.spike_deaths_record=global.spike_deaths;
            global.spike_orbs_record=global.spike_orbs;
            global.spike_orb_total_record=global.spike_orb_total;
            global.spike_round_two_record=global.spike_round_two;
            global.steps_record=global.steps;
            global.seconds_record=global.seconds;
            global.minutes_record=global.minutes;
            global.hours_record=global.hours;
            
            global.spike_challenge=false;
            }
        // Resetting
        global.steps=0; global.seconds=0; global.minutes=0; global.hours=0; global.spike_round_two=false;
        global.spike_level=-1; global.spike_jumps=0;  global.spike_deaths=0; global.spike_orbs=0; //global.spike_level must equal -1 for the menu
        }
    } //going back to the main menu is handled by the normal end of level code, as it has to wait for the game to finish unloading
    
draw_angle+=(vspd/S_MAX_V)*2;
