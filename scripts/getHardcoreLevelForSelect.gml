///getHardcoreLevel()
if(global.round_two) {
    global.select1 = 1;
}

if(room == rm_menu || room == rm_intro || room = rm_transition0 || room == rm_transition) {
    return -1;
} else if(room == rm_level01) {
    return 0;
} else if(room == rm_level0) {
    return 1;
} else {
    return real(string_copy(room_get_name(room), 9, string_length(room_get_name(room)) - 8)) + 1;
}
