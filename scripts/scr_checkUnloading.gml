///scr_checkUnloading() - returns true if the player is within the end region

if(instance_exists(obj_player)) {
    if(obj_player.x>80 && obj_player.x<160 && obj_player.y>420) {
        return true;
    }
}

return false;
