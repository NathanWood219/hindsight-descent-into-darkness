///scr_asciiToText(int ascii) - returns a string

switch(argument0) {
    case 8: return "Backspace";
    case 9: return "Tab";
    case 13: return "Enter";
    case 27: return "Escape";
    case 32: return "Space";
    case 37: return "Left Arrow";
    case 38: return "Up Arrow";
    case 39: return "Right Arrow";
    case 40: return "Down Arrow";
    default: return chr(argument0);
}
