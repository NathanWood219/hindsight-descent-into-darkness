///scr_vibrateController(leftMotor, rightMotor, steps)
if(global.gamepad_vibration) {
    gamepad_set_vibration(global.device, argument0, argument1);
    vibrateSteps = argument2;
}
