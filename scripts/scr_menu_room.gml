//Initial math

var accel, fric;
if (on_ground) {
    accel = S_RUN_ACCEL;
    fric = S_RUN_FRIC;
} else {
    accel = S_AIR_ACCEL;
    fric = S_AIR_FRIC;
}

//Running left
if(hspd>0)
    {hspd = approach( hspd, 0, fric )}
        
hspd = approach( hspd, -S_MAX_H, accel ); 
image_xscale=-1; standing=false;

if(x<-20)
    {room_goto_next()}
    
if(image_alpha<1)
    {image_alpha+=0.05}
    
//Jumping and gravity
if(on_ground)
    {if(place_meeting(x-60,y,obj_solid_parent))
        {vspd = S_JUMP_SPEED; standing=false; crouching=false; menuLanded = false}
    }
else
    {vspd = approach( vspd, S_MAX_V, S_GRAVITY )}
    
//Vibration fix
if(!menuLanded && place_meeting(x,y+1,obj_solid_parent)) {
    scr_vibrateController(0.3, 0.3, 10);
    menuLanded = true;
}
