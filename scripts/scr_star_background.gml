///Background
background_visible[0]=true;
global.sky_type = 1; //Set this engines sky.
system = part_system_create()

star = part_type_create()
part_type_blend(star,true)
part_type_color_mix(star,c_white,make_color_rgb(234,232,255))
part_type_shape(star,pt_shape_pixel)
part_type_size(star,0.2,0.5,0,0.02)
part_type_life(star,9999999,9999999)

emitter = part_emitter_create(system)
part_emitter_region(system,emitter,0,room_width,0,room_height,ps_shape_rectangle,ps_distr_linear)
part_emitter_burst(system,emitter,star,200)
