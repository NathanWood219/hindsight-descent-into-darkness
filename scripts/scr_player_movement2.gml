/*      PLAYER INPUT EVAL       */

var accel, fric;
if(on_ground)
    {
    accel = S_RUN_ACCEL;
    fric = S_RUN_FRIC;
    }
else
    {
    accel = S_AIR_ACCEL;
    fric = S_AIR_FRIC;
    }

if!(can_move) and !(room=rm_intro) and !(room=rm_menu) and !(room=rm_transition) and !(room=rm_transition0)
    {hspd = approach( hspd, 0, fric )}

if!(crouching)
    {
    if(keyboard_check_direct( key_left ) or gamepad_axis_value(device,gp_axislh)<=-0.7) and (can_move)
        {
        //Running left
        
        //First add friction if currently running right
        if(hspd<0)
            {hspd = approach( hspd, 0, fric )}
            
        hspd = approach( hspd, S_MAX_H, accel ); 
        image_xscale=1; standing=false;
    
        }
    else if(keyboard_check_direct( key_right ) or gamepad_axis_value(device,gp_axislh)>=0.7) and (can_move)
        {
        //Running right
        
        //First add friction if currently running left
        if(hspd>0)
            {hspd = approach( hspd, 0, fric )}
            
        hspd = approach( hspd, -S_MAX_H, accel ); 
        image_xscale=-1; standing=false;
    
        }
    else
        {
        //Stopping
    
        hspd = approach( hspd, 0, fric );
    
        }
    
    
    if(on_ground)
        {
    
        candj = true;
        
        //Jumping
        if(keyboard_check_pressed( key_jump ) or gamepad_button_check_pressed(device,gp_face1)) and (can_move) and (can_jump)
            {
            vspd = S_JUMP_SPEED; 
            standing=false; crouching=false;
            global.beta_jumps+=1; // adding to the global tracker
            
            //adding to level stats
            if(global.spike_challenge)
                {addHardcoreStat("jumps")}
            else
                {global.level_array[global.level,4]+=1}
            }
        }
    else
        {
    
        //Gravity
        if!(global.cutscene)
            {vspd = approach( vspd, S_MAX_V, S_GRAVITY )}
            
        //Double jumping
        if(keyboard_check_pressed( key_jump ) or gamepad_button_check_pressed(device,gp_face1)) and (can_move)
            {if(candj) and !(global.loading) and !(global.unloading)
                {
                candj = false;
                vspd = S_DJUMP_SPEED;
                image_index=12; image_speed=1; jumping=true;
                standing=false; crouching=false;
                
                global.beta_jumps+=1; // adding to the global tracker
                
                //adding to level stats
                if(global.spike_challenge)
                    {addHardcoreStat("jumps"); addHardcoreStat("doubleJumps")}
                else
                    {global.level_array[global.level,4]+=1}
                }
            }
        }
    }
