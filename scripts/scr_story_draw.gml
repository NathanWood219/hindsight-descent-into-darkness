draw_set_font(fnt_tutorial);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);

draw_sprite_ext(spr_circle2,0,x,y,0.3,0.3,angle,c_white,alpha+extra_alpha);

if(global.round_two)
    {text_angle=180}
else
    {text_angle=0}

if(alpha>0) and (has_loaded) and !(global.unloading) and !(global.draw_menu)
    {if!(room=rm_level23)
        {draw_text_transformed_color(room_width/2+diff,room_height-50,text,1,1,text_angle,c_white,c_white,c_white,c_white,alpha)}
    else if(global.round_two)
        {draw_text_transformed_color(260,430,text,1,1,text_angle,c_white,c_white,c_white,c_white,alpha)}
    else
        {draw_text_transformed_color(200,430,text,1,1,text_angle,c_white,c_white,c_white,c_white,alpha)}
    }
