image_speed=0;
sprite_index=spr_decor_mask3;
x+=10; y+=10;
selected=false;

if(place_meeting(x,y,obj_spikes) or place_meeting(x,y,obj_spikes_fadein)) and (global.spike_challenge)
    {instance_destroy()}

if(object_index=obj_decor)
    {index=obj_floor}
else if(object_index=obj_decor_fadein)
    {index=obj_solid_parent}
else if(object_index=obj_decor_fadeout)
    {index=obj_floor_fadeout}

if(place_meeting(x+5,y,index)) and (place_meeting(x-5,y,index)) and (selected=false)
    {sprite_index=spr_decor_hole; image_index=random(image_number);
    if(place_meeting(x,y+5,index)) and (selected=false)
        {image_angle=0; selected=true}
    if(place_meeting(x,y-5,index)) and (selected=false)
        {image_angle=180; selected=true}
    }
if(place_meeting(x,y+5,index)) and (place_meeting(x,y-5,index)) and (selected=false)
    {sprite_index=spr_decor_hole; image_index=random(image_number);
    if(place_meeting(x+5,y,index)) and (selected=false)
        {image_angle=90; selected=true}
    if(place_meeting(x-5,y,index)) and (selected=false)
        {image_angle=270; selected=true}
    }
if(place_meeting(x+5,y,index)) and (selected=false)
    {sprite_index=spr_decor_corner; image_index=random(image_number);
    if(place_meeting(x,y+5,index)) and (selected=false)
        {image_angle=0; selected=true}
    if(place_meeting(x,y-5,index)) and (selected=false)
        {image_angle=90; selected=true}
    }
if(place_meeting(x-5,y,index)) and (selected=false)
    {sprite_index=spr_decor_corner; image_index=random(image_number);
    if(place_meeting(x,y+5,index)) and (selected=false)
        {image_angle=270; selected=true}
    if(place_meeting(x,y-5,index)) and (selected=false)
        {image_angle=180; selected=true}
    }
    
if(selected=false)
    {if(floor(random(50))=0)
        {sprite_index=spr_decor_ground1; image_index=0; alive = true}
    else
        {sprite_index=spr_decor_ground0; image_index=random(image_number)}
    }
if(place_meeting(x,y+5,index)) and (selected=false)
    {image_angle=0; selected=true}
if(place_meeting(x,y-5,index)) and (selected=false)
    {image_angle=180; selected=true}
if(place_meeting(x+5,y,index)) and (selected=false)
    {image_angle=90; selected=true}
if(place_meeting(x-5,y,index)) and (selected=false)
    {image_angle=270; selected=true}
    
if(selected=false)
    {instance_destroy()}
    
visible=true;
