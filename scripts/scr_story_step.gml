if(distance_to_object(obj_player)<15) and (obj_player.image_alpha=1)
    {if(alpha<1)
        {alpha+=0.02}
    }
else
    {if(alpha>0)
        {alpha-=0.02}
    }
    
if(alpha>1)
    {alpha=1}
if(alpha<0)
    {alpha=0}
    
//easter egg
if(place_meeting(x,y,obj_player))
    {play_sound=false;
    if(obj_player.image_index=12)
        {angle-=alpha+1}
    else if(obj_player.image_index=13)
        {angle-=alpha+2}
    else if(obj_player.image_index=14)
        {angle-=alpha+3; play_sound=true}
    else if(obj_player.image_index=15)
        {angle-=alpha+2}
    else if(obj_player.image_index=16)
        {angle-=alpha+1}
    else
        {angle-=alpha}
    
    if!(audio_is_playing(snd_whir)) and (play_sound)
        {audio_play_sound(snd_whir,1,true)}
    if!(play_sound) and (audio_is_playing(snd_whir))
        {audio_stop_sound(snd_whir)}
    }
else
    {angle-=alpha}
    
if!(global.loading) and (image_alpha<1) and !(has_loaded)
    {alpha+=0.02}
    
if!(global.loading) and (image_alpha=1) and !(has_loaded)
    {has_loaded=true}
    
if(global.unloading)
    {visible=false}
