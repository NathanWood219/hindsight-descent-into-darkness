///stringPad(value, paddingLength)
var output;

output = string(argument0);

while(string_length(output) < argument1) {
    output = "0" + output;
}

return output;
