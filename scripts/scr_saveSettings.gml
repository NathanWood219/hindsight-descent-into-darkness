ini_open("settings.ini");

ini_write_real("options","fireflies",global.fireflies);
ini_write_real("options","particles",global.particles);
ini_write_real("options","body_parts",global.body_parts);
ini_write_real("options","fast_spawn",global.fast_spawn);
ini_write_real("options","hardcore_timer",global.hardcore_timer);
ini_write_real("options","fullscreen",global.fullscreen);
ini_write_real("options","vibration",global.extra_bool1);
ini_write_real("options","snd_volume",global.snd_volume);
ini_write_real("options","msc_volume",global.msc_volume);

ini_close();
