check1=global.menu;

if(global.menu=0) //main menu
    {if(global.select=0) //level select
        {global.menu=1; global.select=0;
        if(global.spike_challenge)
            {global.select1=0}
        }
    else if(global.select=1) //hardcore
        {if(global.orbs_collected=global.total_orbs) //making sure that it is unlocked
            {global.menu=2; global.select=0; global.select1 = 0}
        else
            {explain_locked=true; alarm[3]=180}
        }
    else if(global.select=2) //statistics
        {global.menu=3; global.select=0}
    else if(global.select=3) //options
        {global.menu=4; global.select=11}
    else if(global.select=4) //GameJolt
        {if(gj_user == "")
            {global.menu=5; part = 0; gjText = ""; gjPrevText = ""; can_select=false; alarm[0] = 10; keyboard_string = ""}
        else
            {gj_user_logout(); username = ""; token = ""}
        }
    else if(global.select=5) //credits
        {global.menu=6; global.select=0; credits_ychange=0}
    else if(global.select=6) //exit game
        {global.menu=7; global.select=0}
    else if(global.select=7) and (mouse) and !(room=rm_menu) and (mouse_x<(room_width/2)-160 or mouse_x>(room_width/2)+160) // clicking with the mouse to return to the game
        {
        instance_activate_all(); //reactivating all the deactivated objects from above
        
        global.draw_menu=false; global.menu=0; global.select=0; alarm[1]=10;
        }
    mouse_select=global.select;
    }
else if(global.menu=1) //level select
    {if!(mouse)
        {scr_goto_level()}
    else
        {
        if(mouse_level_goto)
            {audio_play_sound(snd_menu_confirm,1,false); scr_goto_level()}
        else if(mouse_go_up) and !(global.level_select=0)
            {audio_play_sound(snd_menu_confirm,1,false); global.level_select-=1;
            
            //making it so you can't do game modes on the menu level
            if(global.level_select=0) and !(global.select1=0)
                {
                if(global.select1=1)
                    {global.select1=0}
                else if(global.select1=2) or (global.select1=3)
                    {global.select1=1}
                }
            }
        else if(mouse_go_down) and !(global.level_select=global.level_select_max)
            {audio_play_sound(snd_menu_confirm,1,false); global.level_select+=1;
            
            //switching back to the correct game modes
            if(global.level_select=1) and !(global.select1=0)
                {
                if(global.select1=1)
                    {global.select1=2}
                }
            }
        else if(global.select1_max=1) and !(global.level_select=0)
            {
            scr_mouse_select((room_width/2)-(60*invert),(room_height/2)+(120*invert),59,20,0);
            scr_mouse_select((room_width/2)+(60*invert),(room_height/2)+(120*invert),59,20,1);
            }
        else if!(global.select=0)
            {
            scr_mouse_select((room_width/2)-(230*invert),(room_height/2)+(120*invert),59,20,0);
            scr_mouse_select((room_width/2)-(110*invert),(room_height/2)+(120*invert),59,20,1);
            scr_mouse_select((room_width/2)+(110*invert),(room_height/2)+(120*invert),59,20,2);
            scr_mouse_select((room_width/2)+(230*invert),(room_height/2)+(120*invert),59,20,3);
            }
        else
            {
            scr_mouse_select((room_width/2)-(70*invert),(room_height/2)+(120*invert),59,20,0);
            scr_mouse_select((room_width/2)+(70*invert),(room_height/2)+(120*invert),59,20,1);
            }
        }
    }
else if(global.menu=2) //hardcore
    {
    if!(mouse)
        {if(global.select1 == 2) {
            global.menu = 2.1;
            global.select = 0;
        } else {
            hardcoreGotoLevel();
        }
    }
    else
        {
        if(global.select == 0) {
            global.menu = 0;
            global.select = 1;
        }
        else if(mouse_level_goto)
            {audio_play_sound(snd_menu_confirm,1,false); hardcoreGotoLevel()}
        else if(mouse_go_up) and !(hardcore_level_select=0)
            {audio_play_sound(snd_menu_confirm,1,false); hardcore_level_select-=1}
        else if(mouse_go_down) and !(hardcore_level_select=24)
            {audio_play_sound(snd_menu_confirm,1,false); hardcore_level_select+=1}
        else
            {
            scr_mouse_select((room_width/2)-(120*invert),(room_height/2)+(120*invert),59,20,0);
            scr_mouse_select((room_width/2)+(0*invert),(room_height/2)+(120*invert),59,20,1);
            scr_mouse_select((room_width/2)+(120*invert),(room_height/2)+(120*invert),59,20,2);
            }
        }
        
        // Old hardcore menu confirm
        /*
        if(global.spike_level=-1) //starting a new run
            {global.menu=0; global.select=0; global.spike_challenge=true; global.amnesia=false; global.start_playing=true; global.draw_menu=false; global.spike_round_two=false; room_goto(rm_menu)}
        else //continuing a previous run
            {
            global.round_two = global.spike_round_two;
            
            global.spike_challenge=true; global.amnesia=false;
            global.menu=0; global.select=0; global.prev_x=0;
            global.draw_menu=false;
        
            if(global.spike_level > 24) {
                level = 50 - global.spike_level;
            } else {
                level = global.spike_level;
            }
            
            switch(level)
                {
                case 0: room_goto(rm_level01); break;
                case 1: room_goto(rm_level0); break;
                case 2: room_goto(rm_level1); break;
                case 3: room_goto(rm_level2); break;
                case 4: room_goto(rm_level3); break;
                case 5: room_goto(rm_level4); break;
                case 6: room_goto(rm_level5); break;
                case 7: room_goto(rm_level6); break;
                case 8: room_goto(rm_level7); break;
                case 9: room_goto(rm_level8); break;
                case 10: room_goto(rm_level9); break;
                case 11: room_goto(rm_level10); break;
                case 12: room_goto(rm_level11); break;
                case 13: room_goto(rm_level12); break;
                case 14: room_goto(rm_level13); break;
                case 15: room_goto(rm_level14); break;
                case 16: room_goto(rm_level15); break;
                case 17: room_goto(rm_level16); break;
                case 18: room_goto(rm_level17); break;
                case 19: room_goto(rm_level18); break;
                case 20: room_goto(rm_level19); break;
                case 21: room_goto(rm_level20); break;
                case 22: room_goto(rm_level21); break;
                case 23: room_goto(rm_level22); break;
                case 24: room_goto(rm_level23); break;
                }
            }
            
    else if(global.select=1) //checking personal record
        {global.menu=2.1; global.select=0}
    else if(global.select=2) //erasing and starting a new run
        {global.menu=2.2; global.select=0}
    else if(global.select=3) //returning to the menu
        {global.menu=0; global.select=1}
    */
    can_confirm=false; alarm[1]=10;
    }
else if(global.menu=2.1) {
    if(global.select == 0) {
        global.hardcore_run = true;
        hardcoreGotoLevel();
    } else if(global.select=1) //checking personal record
        {global.menu=2.2; global.select=0}
    else if(global.select=2) //erasing and starting a new run
        {global.menu=2.3; global.select=0}
    else if(global.select=3) //returning to the menu
        {global.menu=2; global.select1 = 2}
    }
else if(global.menu=2.2) //checking personal record in hardcore mode
    {}
else if(global.menu=2.3) //confirmation for resetting hardcore mode run, first option = no, second option = yes
    {if(global.select=0)
        {global.menu=2; global.select=2}
    else if(global.select=1)
        {
        //checking for a new record
        if(global.spike_level>global.spike_level_record) and (global.spike_level>=0)
            {
            //setting new records
            global.spike_level_record=global.spike_level;
            global.spike_jumps_record=global.spike_jumps;
            global.spike_deaths_record=global.spike_deaths;
            global.spike_orbs_record=global.spike_orbs;
            global.spike_round_two_record=global.spike_round_two;
            global.steps_record=global.steps;
            global.seconds_record=global.seconds;
            global.minutes_record=global.minutes;
            global.hours_record=global.hours;
            }
        
        global.steps=0; global.seconds=0; global.minutes=0; global.hours=0; global.spike_round_two=false;
        global.spike_level=-1; global.spike_jumps=0;  global.spike_deaths=0; global.spike_orbs=0; //global.spike_level must equal -1 for the menu
        
        global.amnesia=true; global.round_two=false;
        
        global.menu=2; global.select=0;
        
        global.spike_level = 0;
        global.hardcore_run = true;
        hardcoreGotoLevel();
        }
    }
else if(global.menu=3) //statistics
    {global.menu=0; global.select=2}
else if(global.menu=4) //options
    {if(global.select=0)
        {global.fireflies = !global.fireflies}
    else if(global.select=1)
        {global.particles = !global.particles}
    else if(global.select=2)
        {global.body_parts = !global.body_parts}
    else if(global.select=3)
        {global.fast_spawn = !global.fast_spawn}
    else if(global.select=4)
        {global.hardcore_timer = !global.hardcore_timer}
    else if(global.select=5)                                    // gamepad vibration patched in
        {global.gamepad_vibration = !global.gamepad_vibration;
    }
    else if(global.select=6)
        {
        global.can_check=false; alarm[2]=6;
        global.fullscreen = !global.fullscreen;
        }
    else if(global.select==9)
        {global.menu = 4.1; global.select = 9}
    else if(global.select=10)
        {global.menu=4.2; global.select=0}
    else if(global.select=11)
        {
        global.menu=0; global.select=3;
        }
    
    }
else if(global.menu=4.1) //rebinding keys
    {
    if(global.select < 8) {
        rebinding = true;
        can_confirm = false;
        can_select = false;
        alarm[1] = 10;
    } else if(global.select == 8) {
        scr_defaultKeys();
    } else if(global.select == 9) {
        global.menu = 4; global.select = 9;
    }
    
    }
else if(global.menu=4.2) //confirmation for resetting the game, first option = no, second option = yes
    {if(global.select=0)
        {global.menu=4; global.select=8}
    else if(global.select=1)
        {if(file_exists("savedata.ini"))
            {file_delete("savedata.ini")}
        game_restart();
        }
    }
else if(global.menu=5) //GameJolt stuff, handled elsewhere
    {}
else if(global.menu=6) //returning from the credits
    {global.menu=0; global.select=5}
else if(global.menu=7) //confirmation for exiting the game, first option = no, second option = yes
    {if(global.select=0)
        {global.menu=0; global.select=6}
    else if(global.select=1)
        {scr_newSaveGame(); game_end()}
    }
if!(global.menu=check1)
    {can_confirm=false; alarm[1]=10;
    audio_play_sound(snd_menu_confirm,1,false);
    instance_create(0,0,obj_short_flash);
    }
