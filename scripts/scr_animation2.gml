///Animating the player
//Optimized version from Flashback

//jumping
if!(place_meeting(x,y-1,obj_solid_parent)) and !(jumping)
    {jumping=true; image_index=12; image_speed=1; standing=false; crouching=false; walking=false; running=false; blinking=false}
if(image_index=22)
    {image_speed=0; image_index=22; jumping=true; standing=false; crouching=false; walking=false; running=false; blinking=false}
if(place_meeting(x,y-1,obj_solid_parent)) and (jumping)
    {jumping=false}
    
//crouching
if(place_meeting(x,y-1,obj_solid_parent)) and !(crouching) and (hspd=0)
    {if(keyboard_check_direct(key_down)) or (gamepad_axis_value(device,gp_axislv)>=0.9)
        {image_index=12; image_speed=1; crouching=true; standing=false; walking=false; blinking=false}
    }
if(crouching)
    {if(image_index=16)
        {image_index=23; image_speed=0; crouching=false}
    if!(keyboard_check_direct(key_down)) and (gamepad_axis_value(device,gp_axislv)<0.9)
        {image_speed=1}
    else if(keyboard_check_direct(key_down)) or (gamepad_axis_value(device,gp_axislv)>=0.9)
        {if(image_index=14)
            {image_speed=0}
        }
    }

//walking right and left
if(hspd>0) and !(jumping) and !(crouching) //right
    {if(place_meeting(x,y-1,obj_solid_parent)) and !(walking)
        {image_index=0; image_speed=0.5; walking=true; standing=false; running=false; blinking=false}
    }
if(hspd<0) and !(jumping) and !(crouching) //left
    {if(place_meeting(x,y-1,obj_solid_parent)) and !(walking)
        {image_index=0; image_speed=0.5; walking=true; standing=false; running=false; blinking=false}
    }
if(walking) and (image_index=5)
    {image_index=0}

/*
//running right and left
if(hspd>=2.8) or (hspd<=-2.8)
    {if(place_meeting(x,y+1,obj_solid_parent)) and !(running) and !(jumping) and !(crouching)
        {image_index=6; image_speed=0.5; running=true; walking=false; standing=false}
    }
if(running) and (image_index=11)
    {image_index=6}
*/
    
//standing and idle animation

if(place_meeting(x,y-1,obj_solid_parent)) and !(jumping) and !(crouching) and (hspd=0) and !(standing) and !(blinking)
    {image_index=23; image_speed=0; walking=false; running=false; standing=true}
    
if(standing=true)
    {if(image_index=23) and (can_blink=true)
        {if(floor(random(200))=0)
            {image_speed=1; image_index=24; can_blink=false; blinking=true; alarm[0]=200}
        }
    }
    
if(blinking) and (image_index=idle_end)
    {image_index=23; image_speed=0; blinking=false}
