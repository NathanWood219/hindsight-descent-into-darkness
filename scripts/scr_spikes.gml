//OPTIMIZED --- now checks to see if it should wrap around corners. it will only wrap if it finds more spikes to connect to

image_speed=0;
sprite_index=spr_spikes;
selected=false;
extended=false; //whether the spike has a larger hitbox or not
extended_mask=spr_spike_mask;
x+=10; y+=10;

if(object_index=obj_spikes)
    {index=obj_floor}
else if(object_index=obj_hardcore_spikes)
    {index=obj_floor}
else if(object_index=obj_hardcore_spikes_fadein)
    {index=obj_solid_parent}
else if(object_index=obj_spikes_fadein)
    {index=obj_solid_parent}
else if(object_index=obj_spikes_fadeout)
    {index=obj_wall_parent}
else if(object_index=obj_spikes_fadeout2)
    {index=obj_floor_fadeout}

//narrow walled spikes
if(place_meeting(x+20,y,index)) and (place_meeting(x-20,y,index)) and !(place_meeting(x,y+20,index)) and !(place_meeting(x,y-20,index)) and (selected=false)
    {image_index=2; image_angle=0; selected=true}
if(place_meeting(x,y+20,index)) and (place_meeting(x,y-20,index)) and !(place_meeting(x+20,y,index)) and !(place_meeting(x-20,y,index)) and (selected=false)
    {image_index=2; image_angle=90; selected=true}

//hole spikes
if(place_meeting(x+20,y,index)) and (place_meeting(x-20,y,index)) and (selected=false)
    {if(place_meeting(x,y+20,index)) and (selected=false)
        {if(place_meeting(x,y-20,object_index))
            {image_index=0; selected=true}
        else
            {image_index=3; selected=true}
        image_angle=0;
        }
    if(place_meeting(x,y-20,index)) and (selected=false)
        {if(place_meeting(x,y+20,object_index))
            {image_index=0; selected=true}
        else
            {image_index=3; selected=true}
        image_angle=180;
        }
    }
if(place_meeting(x,y+20,index)) and (place_meeting(x,y-20,index)) and (selected=false)
    {if(place_meeting(x+20,y,index)) and (selected=false)
        {if(place_meeting(x-20,y,object_index))
            {image_index=0; selected=true}
        else
            {image_index=3; selected=true}
        image_angle=90;
        }
    if(place_meeting(x-20,y,index)) and (selected=false)
        {if(place_meeting(x+20,y,object_index))
            {image_index=0; selected=true}
        else
            {image_index=3; selected=true}
        image_angle=270;
        }
    }

//corner spikes
if(place_meeting(x+20,y,index)) and (selected=false)
    {if(place_meeting(x,y+20,index)) and (selected=false)
        {image_angle=0; mask_index=spr_spike_mask_corner; image_index=1; selected=true;
        }
    if(place_meeting(x,y-20,index)) and (selected=false)
        {image_angle=90; mask_index=spr_spike_mask_corner; image_index=1; selected=true;
        }
    }
if(place_meeting(x-20,y,index)) and (selected=false)
    {if(place_meeting(x,y+20,index)) and (selected=false)
        {image_angle=270; mask_index=spr_spike_mask_corner; image_index=1; selected=true;
        }
    if(place_meeting(x,y-20,index)) and (selected=false)
        {image_angle=180; mask_index=spr_spike_mask_corner; image_index=1; selected=true;
        }
    }
    
//side spikes
if(selected=false)
    {image_index=3}

//filling to the edge of the blocks
if(place_meeting(x,y+20,index)) and (selected=false)
    {if!(place_meeting(x-20,y+20,index)) and !(place_meeting(x+20,y+20,index))
        {mask_index=spr_spike_mask_large; extended=true}
    else if(place_meeting(x-20,y+20,index)) and !(place_meeting(x+20,y+20,index))
        {mask_index=spr_spike_mask_right; extended=true}
    else if(place_meeting(x+20,y+20,index)) and !(place_meeting(x-20,y+20,index))
        {mask_index=spr_spike_mask_left; extended=true}
    else
        {mask_index=spr_spike_mask}
    image_angle=0; selected=true;
    
    //make spikes spread to nearby spikes if they exist
    if(place_meeting(x+20,y,obj_spikes)) and (place_meeting(x-20,y,obj_spikes))
        {mask_index=spr_spike_mask_large}
    }
if(place_meeting(x,y-20,index)) and (selected=false)
    {if!(place_meeting(x-20,y-20,index)) and !(place_meeting(x+20,y-20,index))
        {mask_index=spr_spike_mask_large; extended=true}
    else if(place_meeting(x-20,y-20,index)) and !(place_meeting(x+20,y-20,index))
        {mask_index=spr_spike_mask_left; extended=true}
    else if(place_meeting(x+20,y-20,index)) and !(place_meeting(x-20,y-20,index))
        {mask_index=spr_spike_mask_right; extended=true}
    else
        {mask_index=spr_spike_mask}
    
    image_angle=180; selected=true;
    
    //make spikes spread to nearby spikes if they exist
    if(place_meeting(x+20,y,obj_spikes)) and (place_meeting(x-20,y,obj_spikes))
        {mask_index=spr_spike_mask_large}
    }

extended_mask=mask_index;
    
if(place_meeting(x+20,y,index)) and (selected=false)
    {mask_index=spr_spike_mask; image_angle=90; selected=true}
if(place_meeting(x-20,y,index)) and (selected=false)
    {mask_index=spr_spike_mask; image_angle=270; selected=true}
    
//remove this line if you want to use a different sprite for hole spikes
if(image_index=0) and (selected)
    {image_index=3}
    
if(selected=false)
    {image_index=4; mask_index=spr_spike_mask_alone}
    
alarm[0]=5;
