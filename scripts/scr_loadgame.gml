if(file_exists("settings.ini"))
    {ini_open("settings.ini");
    
    //changing option variables
    fireflies=ini_read_real("options","fireflies",1);
    particles=ini_read_real("options","particles",1);
    body_parts=ini_read_real("options","body_parts",1);
    fast_spawn=ini_read_real("options","fast_spawn",0);
    fullscreen=ini_read_real("options","fullscreen",0);
    
    //Only change if they are not the defaults
    if(fireflies=0)
        {global.fireflies=false}
    if(particles=0)
        {global.particles=false}
    if(body_parts=0)
        {global.body_parts=false}
    if(fast_spawn=1)
        {global.fast_spawn=true}
    if(fullscreen=1)
        {global.fullscreen=true}
    
    global.snd_volume=ini_read_real("options","snd_volume",1);
    global.msc_volume=ini_read_real("options","msc_volume",1);
    
    ini_close();
    }
