if(gj_user != "") {
    if(global.achieve_partone) {
        gj_trophy_add("48937");
    }
    
    if(global.achieve_parttwo) {
        gj_trophy_add("48938");
    }
    
    if(global.achieve_orbs1) {
        gj_trophy_add("48939");
    }
    
    if(global.achieve_orbs2) {
        gj_trophy_add("48940");
    }
    
    if(global.achieve_hardcore) {
        gj_trophy_add("48941");
    }
    
    if(global.achieve_hardcoreorbs1) {
        gj_trophy_add("48942");
    }
    
    if(global.achieve_hardcoreorbs2) {
        gj_trophy_add("48943");
    }
    
    if(global.achieve_hardcoretimed) {
        gj_trophy_add("48944");
    }
    
    if(global.spike_level_record >= 50) {
        if(global.hours_record<=9)
            {hour_string="0"}
        else
            {hour_string=""}

        if(global.minutes_record<=9)
            {min_string="0"}
        else
            {min_string=""}
        
        if(global.seconds_record<=9)
            {sec_string="0"}
        else
            {sec_string=""}
        
        timeString = hour_string + string(global.hours_record) + ":" + min_string + string(global.minutes_record) + ":" + sec_string + string(global.seconds_record);
        timeReal = (global.hours_record*60*60*60) + (global.minutes_record*60*60) + (global.seconds_record*60) + (global.steps_record);
        extraData = "Jumps: " + string(global.spike_jumps_record) + ", Deaths: " + string(global.spike_deaths_record) + ", Orbs: " + string(global.spike_orbs_record) + "/50";
        
        gj_scores_add("124427", timeString, timeReal, extraData);
    }
}
