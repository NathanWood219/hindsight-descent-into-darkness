///hardcoreGotoLevel()

var levelGoto = 0;

global.spike_challenge = true;

if(global.menu == 2.1) {
    if(global.spike_level > 24) {
        levelGoto = 50 - global.spike_level;
    } else {
        levelGoto = global.spike_level;
    }
        
    global.hardcore_run = true;
    global.round_two = global.spike_round_two;
    
} else {
    if(global.select1 == 0) {
        global.round_two = false;
        levelGoto = hardcore_level_select;
    } else if(global.select1 == 1) {
        global.round_two = true;
        levelGoto = hardcore_level_select;
    } else {
        global.menu = 2.1;
        global.select = 0;
    }
    
    global.hardcore_run = false;
}

global.spike_challenge = true;
global.amnesia = false;
global.menu = 0;
global.select = 0;
global.prev_x = 0;
global.draw_menu = false;

if(global.hardcore_run && levelGoto == -1) {
    room_goto(rm_level01);
} else if(levelGoto == 0) {
    room_goto(rm_level01);
} else if(levelGoto == 1) {
    room_goto(rm_level0);
} else {
    room_goto(asset_get_index("rm_level" + string(levelGoto-1)));
}
