//drawing the return text                
draw_set_font(fnt_storyline);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
                
if(global.controller)
    {return_text="B"}
else
    {return_text=scr_asciiToText(key_return)}
    
if(mouse)
    {scr_text_alpha_mouse((room_width/2),(room_height/2)+(235*invert),200,20)}
else
    {text_alpha=0.4; mouse_go_back=false}
    
if(text_alpha=0.9)
    {mouse_go_back=true}
else
    {mouse_go_back=false}
        
if!(mouse)
    {draw_text_transformed_colour((room_width/2),(room_height/2)+(235*invert),"Press " + return_text + " to return to the menu",1,1,text_angle,c_white,c_white,c_white,c_white,text_alpha)} //telling the player how to return
else
    {if!(global.menu=0)
        {draw_text_transformed_colour((room_width/2),(room_height/2)+(235*invert),"Return to menu",1,1,text_angle,c_white,c_white,c_white,c_white,text_alpha)}
    else
        {draw_text_transformed_colour((room_width/2),(room_height/2)+(235*invert),"Return to game",1,1,text_angle,c_white,c_white,c_white,c_white,text_alpha)}
    }
