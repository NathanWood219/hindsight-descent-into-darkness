///scr_levelDataRead(int level, string data)
data = argument1;

for(k=0; k<5; k++) {
    // Skip 3 as that level stat is not used
    if(k == 3) {
        k++;
    }

    close = string_pos(data, " ");
    global.level_array[argument0, k] = real(string_copy(data, 1, close - 1));
    data = string_copy(data, close + 1, string_length(data) - close);
}


/*
for(k=0; k<5; k++) {
    // Skip 3 as that level stat is not used
    if(k == 3) {
        k++;
    }

    close = string_pos(data, " ");
    global.level_array[argument0, k] = real(string_copy(data, 1, close - 1));
    data = string_copy(data, close + 1, string_length(data) - close);
}
