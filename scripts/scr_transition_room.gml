/*

global.transition_stage

Stage 0: falling into the hole, move towards center of screen
Stage 1: after hitting the ground, shake the screen for a bit
Stage 2: flash the screen, it is now flipped over
Stage 3: waiting for alarm[4]
Stage 4: falling in reverse

Creation code of previous level must reset the Stage=0

obj_screen_shake handles all of Stages 1-2.

*/

if(global.transition_stage=0) and !(y>312)
    {image_index=22; image_speed=0;
    if(x>=room_width/2)
        {draw_angle+=1}
    else if(x<room_width/2)
        {draw_angle-=1}
    }
    
if(global.transition_stage=0) and (y>312) {
    global.transition_stage=1; can_move=false;
}
    
if(global.transition_stage=1) and (instance_number(obj_screen_shake)=0)
    {audio_play_sound(snd_landing,1,false); instance_create(0,0,obj_screen_shake); audio_play_sound(snd_transition,1,false)}
    
if!(global.transition_stage=2) and (y>312)
    {vspd=0; vspeed=0; gravity=0; can_move=false; global.loading=false}
    
if(global.transition_stage=2)
    {image_index=22; image_speed=0;
        
    view_angle[0]=180;
        
    if!(global.round_two=true)
        {global.round_two=true}
        
    audio_play_sound(snd_explosion,1,false);
        
    alarm[4]=20;
    global.transition_stage=3;
    }
    
if(global.transition_stage=4)
    {if!(vspd=5)
        {vspd+=1}
        
    if(x>=room_width/2)
        {draw_angle+=1}
    else if(x<room_width/2)
        {draw_angle-=1}
    }
    
if!(global.transition_stage=4)
    {global.unloading=false}
