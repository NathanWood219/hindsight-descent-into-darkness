if(!file_exists("savedata.ini")) {
    exit;
}

ini_open("savedata.ini");

// Statistics
global.beta_hours = ini_read_real("Statistics", "Hours", 0);
global.beta_minutes = ini_read_real("Statistics", "Minutes", 0);
global.beta_seconds = ini_read_real("Statistics", "Seconds", 0);
global.beta_steps = ini_read_real("Statistics", "Steps", 0);
global.beta_jumps = ini_read_real("Statistics", "Jumps", 0);
global.deaths = ini_read_real("Statistics", "Deaths", 0);
global.orbs_collected = ini_read_real("Statistics", "Orbs", 0);
global.distance_traveled = ini_read_real("Statistics", "Distance", 0);

// Key Bindings
key_left = ini_read_real("Bindings", "Left", vk_left);
key_right = ini_read_real("Bindings", "Right", vk_right);
key_up = ini_read_real("Bindings", "Up", vk_up);
key_down = ini_read_real("Bindings", "Down", vk_down);
key_up = ini_read_real("Bindings", "Jump", vk_up);
key_select = ini_read_real("Bindings", "Select", vk_enter);
key_return = ini_read_real("Bindings", "Return", vk_backspace);
key_start = ini_read_real("Bindings", "Start", vk_escape);

// Settings
global.fireflies = ini_read_real("Options","Fireflies", true);
global.particles = ini_read_real("Options","Particles", true);
global.body_parts = ini_read_real("Options","BodyParts", true);
global.fast_spawn = ini_read_real("Options","FastSpawn", false);
global.hardcore_timer = ini_read_real("Options","HardcoreTimer", true);
global.fullscreen = ini_read_real("Options","Fullscreen", false);
global.gamepad_vibration = ini_read_real("Options","Vibration", true);
global.snd_volume = ini_read_real("Options","SoundVolume", 1);
global.msc_volume = ini_read_real("Options","MusicVolume", 1);

// Achievements
global.achieve_partone = ini_read_real("Achievements", "One", false);
global.achieve_parttwo = ini_read_real("Achievements", "Two", false);
global.achieve_orbs1 = ini_read_real("Achievements", "Three", false);
global.achieve_orbs2 = ini_read_real("Achievements", "Four", false);
global.achieve_hardcore = ini_read_real("Achievements", "Five", false);
global.achieve_hardcoreorbs1 = ini_read_real("Achievements", "Six", false);
global.achieve_hardcoreorbs2 = ini_read_real("Achievements", "Seven", false);
global.achieve_hardcoretimed = ini_read_real("Achievements", "Eight", false);

// Levels Data
for(i=0; i<26; i++) {
    for(k=0; k<6; k++) {
        //Skipping an unused stat
        if(k == 3) {
            k++;
        }
        
        global.level_array[i, k] = ini_read_real("LevelData", "Level" + string(i+1) + "_" + string(k), 0);
    }
}

// Hardcore
global.spike_level = ini_read_real("Hardcore", "Level", -1);
global.spike_jumps = ini_read_real("Hardcore", "Jumps", 0);
global.spike_deaths = ini_read_real("Hardcore", "Deaths", 0);
global.spike_orbs = ini_read_real("Hardcore", "Orbs", 0);
global.spike_orb_total = ini_read_real("Hardcore", "OrbTot", 0);
global.spike_round_two = ini_read_real("Hardcore", "PartTwo", false);
global.hours = ini_read_real("Hardcore", "Hours", 0);
global.minutes = ini_read_real("Hardcore", "Minutes", 0);
global.seconds = ini_read_real("Hardcore", "Seconds", 0);
global.steps = ini_read_real("Hardcore", "Steps", 0);
global.spike_level_record = ini_read_real("Hardcore", "LevelRec", -1);
global.spike_jumps_record = ini_read_real("Hardcore", "JumpsRec", 0);
global.spike_deaths_record = ini_read_real("Hardcore", "DeathsRec", 0);
global.spike_orbs_record = ini_read_real("Hardcore", "OrbsRec", 0);
global.spike_round_two_record = ini_read_real("Hardcore", "PartTwoRec", false);
global.spike_orb_total_record = ini_read_real("Hardcore", "OrbTotRec", 0);
global.hours_record = ini_read_real("Hardcore", "HoursRec", 0);
global.minutes_record = ini_read_real("Hardcore", "MinutesRec", 0);
global.seconds_record = ini_read_real("Hardcore", "SecondsRec", 0);
global.steps_record = ini_read_real("Hardcore", "StepsRec", 0);

// Hardcore levels data
if(ini_key_exists("Hardcore", "LevelsData")) {
    ds_grid_read(global.hardcore_levels, ini_read_string("Hardcore", "LevelsData", "You broke the game, nice."));
}

global.temp_hardcore_select = ini_read_real("Hardcore", "HardcoreSelect", 0);

// Misc
global.level_select = ini_read_real("Misc", "LastLevel", 0);
global.select1 = ini_read_real("Misc", "LastMode", 0);
global.player_skin = ini_read_real("Misc", "PlayerSkin", 0);
gj_user = ini_read_string("Misc", "GJUser", "");
gj_token = ini_read_string("Misc", "GJToken", "");
global.unlocked_no_amnesia = ini_read_real("Misc", "Adventure", false);
global.unlocked_spike_challenge = ini_read_real("Misc", "Hardcore", false);

global.loadedData = true;

ini_close();
